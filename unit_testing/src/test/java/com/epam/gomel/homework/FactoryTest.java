package com.epam.gomel.homework;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import java.time.Month;

public class FactoryTest {
    @Factory(dataProvider = "Boys")
    public Object[] createTest(Month month, double wealth, Girl girl, Mood expected) {
        System.out.println(String.format("Test for Boy %s %s %s", month, wealth, girl));
        return new Object[]{new BoyMoodTest(month, wealth, girl, expected)};
    }

    @DataProvider(name = "Boys")
    public Object[][] valuesForMood() {
        final double  wealth = 1_000_001;
        return new Object[][]{
                {Month.JANUARY, 0, null, Mood.HORRIBLE},
                {Month.JANUARY, wealth, null, Mood.BAD},
                {Month.JUNE, wealth, null, Mood.NEUTRAL},
                {Month.SEPTEMBER, wealth, new Girl(true), Mood.GOOD},
                {Month.JULY, wealth, new Girl(true), Mood.EXCELLENT}
        };
    }

}

package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.epam.gomel.homework.listener.TestListener;
import java.time.Month;

@Listeners(TestListener.class)
public class SpendMoneyTest {
    @Test(groups = "params")
    @Parameters({"amount", "wealth", "month"})
    public void positiveTestSpendMoney(double amount, double wealth, Month month) {
        Boy boy = new Boy(month, wealth);
        boy.spendSomeMoney(amount);
        double expected = wealth - amount;
        Assert.assertEquals(boy.getWealth(), expected, "Method 'spendSomeMoney' working incorrectly");
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void negativeTestSpendMoney() {
        final double lowWealth = 100_000;
        final double amount = 200_000;
        Boy boy = new Boy(Month.JULY, lowWealth);
        boy.spendSomeMoney(amount);
    }
}

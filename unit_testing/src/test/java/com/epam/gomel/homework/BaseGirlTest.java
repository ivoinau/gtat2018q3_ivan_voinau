package com.epam.gomel.homework;

import org.testng.annotations.BeforeTest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BaseGirlTest {

    Boy mockedBoy = mock(Boy.class);

    @BeforeTest
    public void stubbing() {

        when(mockedBoy.isRich()).thenReturn(true);
        when(mockedBoy.isSummerMonth()).thenReturn(true);
    }
}

package com.epam.gomel.homework;

import org.testng.annotations.Test;

import java.time.Month;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class NullPointerTest {
    Girl girl = new Girl();
    Boy boy = new Boy(Month.JANUARY);

    @Test
    public void testGirlGetMood() {
        assertThat(girl.getMood(), is(notNullValue()));
    }

    @Test
    public void testIsBoyfriendRich() {
        assertThat(girl.isBoyfriendRich(), is(notNullValue()));
    }

    @Test
    public void testIsSlimFriendBecameFat() {
        assertThat(girl.isSlimFriendBecameFat(), is(notNullValue()));
    }

    @Test
    public void testIsBoyFriendWillBuyNewShoes() {
        assertThat(girl.isBoyFriendWillBuyNewShoes(), is(notNullValue()));
    }

    @Test
    public void testSpendBoyFriendMoney() {
        final double amount = 100;
        girl.spendBoyFriendMoney(amount);
    }

    @Test
    public void testIsRich() {
        assertThat(boy.isRich(), is(notNullValue()));
    }

    @Test
    public void testBoyGetMood() {
        assertThat(boy.getMood(), is(notNullValue()));
    }

    @Test
    public void testIsPrettyGirlFriend() {
        assertThat(boy.isPrettyGirlFriend(), is(notNullValue()));
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testSpendSomeMoney() {
        final double amount = 100;
        boy.spendSomeMoney(amount);
    }
}

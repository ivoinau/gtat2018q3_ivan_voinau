package com.epam.gomel.homework;

import com.epam.gomel.homework.listener.TestListener;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.time.Month;

@Listeners(TestListener.class)
public class BoyMoodTest {
    private Month month;
    private double wealth;
    private Girl girl;
    private Mood expected;

    public BoyMoodTest(Month month, double wealth, Girl girl, Mood expected) {
        this.month = month;
        this.wealth = wealth;
        this.girl = girl;
        this.expected = expected;
    }

    @Test
    public void testBoy() {
        Boy boy = new Boy(month, wealth, girl);
        Mood mood = boy.getMood();
        Assert.assertEquals(mood, expected, "It is wrong mood");
    }
}

package com.epam.gomel.homework.runner;

import org.testng.TestNG;

import java.util.Arrays;
import java.util.List;

public class TestRunner {
    public static void main(String[] args) {
        TestNG testng = new TestNG();
        List<String> files = Arrays.asList("./src/test/resources/AllTests.xml");
        testng.setTestSuites(files);
        testng.run();
    }
}

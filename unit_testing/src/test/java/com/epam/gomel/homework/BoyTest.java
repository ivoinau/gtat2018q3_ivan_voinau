package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.epam.gomel.homework.listener.TestListener;

import java.time.Month;

@Listeners(TestListener.class)
public class BoyTest {

    @Test(dataProvider = "Month")
    public void testSummer(Month month, boolean expected) {
        Boy boy = new Boy(month);
        boolean isSummer = boy.isSummerMonth();
        Assert.assertEquals(isSummer, expected, "Method 'isSummer' working incorrectly");
    }

    @Test(dataProvider = "MonthAndWealth", priority = 1)
    public void testRich(Month month, double wealth, boolean expected) {
        Boy boy = new Boy(month, wealth);
        boolean isRich = boy.isRich();
        Assert.assertEquals(isRich, expected, "Method 'isRich' working incorrectly");
    }

    @DataProvider(name = "Month")
    public Object[][] valuesForMonth() {
        return new Object[][]{
                {Month.MAY, false},
                {Month.JUNE, true},
                {Month.JULY, true},
                {Month.AUGUST, true},
                {Month.SEPTEMBER, false}
        };
    }

    @DataProvider(name = "MonthAndWealth")
    public Object[][] valuesForMonthAndWealth() {
        final double beforeBoundaryWealth = 999_999;
        final double boundaryWealth = 1_000_000;
        final double afterBoundaryWealth = 1_000_001;
        return new Object[][]{
                {Month.MAY, beforeBoundaryWealth, false},
                {Month.JUNE, boundaryWealth, true},
                {Month.JULY, afterBoundaryWealth, true}
        };
    }
}

package com.epam.gomel.homework;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.epam.gomel.homework.listener.TestListener;

import java.time.Month;

import static org.mockito.Mockito.verify;

@Listeners(TestListener.class)
public class GirlTest extends BaseGirlTest {

    @Test(dataProvider = "Girls", priority = 1)
    public void testGirl(boolean isPreety, boolean isSlimFriendGotAFewKilos, Boy boy, Mood expected) {
        Girl girl = new Girl(isPreety, isSlimFriendGotAFewKilos, boy);
        Mood mood = girl.getMood();
        Assert.assertEquals(mood, expected, "It is wrong mood");
    }

    @Test(groups = "mocked")
    public void isBoyfriendRichTest() {
        Girl girl = new Girl(false, false, mockedBoy);
        boolean isRich = girl.isBoyfriendRich();
        Assert.assertEquals(isRich, true, "method 'isBoyfriendRich' working incorrectly");
    }

    @Test(dataProvider = "IsBuyShoes", groups = "mocked", dependsOnMethods = "isBoyfriendRichTest")
    public void isBoyfriendWillBuyShoes(boolean isPreety, boolean isSlimFriendGotAFewKilos, Boy boy, boolean expected) {
        Girl girl = new Girl(true, false, mockedBoy);
        boolean isBuy = girl.isBoyFriendWillBuyNewShoes();
        Assert.assertEquals(isBuy, true, "method 'isBoyFriendWillBuyNewShoes' working incorrectly");
    }

    @Test(groups = "mocked", priority = 2)
    public void spendBoyFriendMoneyTest() {
        Girl girl = new Girl(true, false, mockedBoy);
        final double amountForSpending = 200_000;
        girl.spendBoyFriendMoney(amountForSpending);
        verify(mockedBoy).spendSomeMoney(amountForSpending);
    }

    @DataProvider(name = "IsBuyShoes")
    public Object[][] valuesForIsBuyShoes() {
        final double lowWealth = 100;
        final double bigWealth = 2_000_000;
        return new Object[][]{
                {false, false, new Boy(Month.JULY, lowWealth), false},
                {true, false, new Boy(Month.JULY, lowWealth), false},
                {false, false, new Boy(Month.JULY, bigWealth), false},
                {true, false, new Boy(Month.JULY, bigWealth), true}
        };
    }

    @DataProvider(name = "Girls")
    public Object[][] valuesForMood() {
        return new Object[][]{
                {false, false, null, Mood.I_HATE_THEM_ALL},
                {false, true, null, Mood.NEUTRAL},
                {true, false, null, Mood.GOOD},
                {false, false, mockedBoy, Mood.GOOD},
                {true, false, mockedBoy, Mood.EXCELLENT}
        };
    }
}

package com.epam.gomel.tat.api;

import api.CurrencyRequest;
import api.WeatherRequest;
import logger.Log;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static api.WeatherRequest.NUMBER_OF_MONTH;
import static io.restassured.RestAssured.given;

public class ApiRequests {

    private static final double CHECK_PERCENT = 0.5;
    private static final int MAX_PERCENT = 100;

    private ArrayList<Document> doc;
    private XPathFactory xPathFactory;
    private XPath xPath;
    private ArrayList<Date> dates;

    @BeforeTest
    public void buildDom() throws ParserConfigurationException, IOException, SAXException, ParseException {
        String url;
        Calendar begin = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        begin.setTime(format1.parse("2017-01-01"));
        end.setTime(format1.parse("2017-01-31"));
        doc = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_MONTH; i++) {
            if (i != 0) {
                begin.add(Calendar.MONTH, 1);
                end.add(Calendar.MONTH, 1);
                end.set(Calendar.DATE, end.getActualMaximum(Calendar.DATE));
            }
            url = "http://api.worldweatheronline.com/premium/v1/past-weather.ashx"
                    + "?key=19d89738e84b4b26ab3100836192801&q=Homyel&format=xml&date="
                    + format1.format(begin.getTime()) + "&enddate=" + format1.format(end.getTime());
            Log.info("Get response for request " + url);
            InputStream response = given().when().get(url).asInputStream();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            doc.add(builder.parse(response));
        }
        xPathFactory = XPathFactory.newInstance();
        xPath = xPathFactory.newXPath();
    }

    @Test
    public void test() throws XPathExpressionException, ParseException {
        double risePercent;
        WeatherRequest weatherRequest = new WeatherRequest();
        dates = new ArrayList<>();
        dates = weatherRequest.getDates(dates, xPath, doc);

        weatherRequest.checkMinTempC(dates, doc.get(0), xPath);
        weatherRequest.checkPrecip(dates, doc.get(0), xPath);

        try {
            risePercent = new CurrencyRequest().getResponse(dates) * MAX_PERCENT;
            Log.info("Chance of rise of currency rate is " + risePercent);
            Assert.assertTrue(risePercent > CHECK_PERCENT, "Currency rate rising possibility is "
                    + risePercent * MAX_PERCENT);
        } catch (IOException e) {
            Log.error(e.getMessage());
        }
    }
}

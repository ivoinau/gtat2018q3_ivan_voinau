package com.epam.gomel.tat.api.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import logger.Log;
import org.apache.log4j.PropertyConfigurator;
import org.testng.TestNG;

import java.util.Collections;

import static com.epam.gomel.tat.api.runner.Parameters.instance;

public class TestRunner {

    public static TestNG createTestNG() {
        TestNG testNG = new TestNG();
        testNG.setTestSuites(Collections.singletonList(instance().getSuite()));
        return testNG;
    }

    public static void main(String[] args) {
        PropertyConfigurator.configure(instance().getLogConfig());
        Log.info("Parse cli");
        parseCli(args);
        Log.info("Start application");
        createTestNG().run();
        Log.info("Finish app");
    }

    private static void parseCli(String[] args) {
        Log.info("Parsing clis using JCommander");
        JCommander jCommander = new JCommander(instance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage());
            System.exit(1);
        }
        if (instance().isHelp()) {
            jCommander.usage();
        }
    }
}

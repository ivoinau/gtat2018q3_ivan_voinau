package com.epam.gomel.tat.api.runner;

import com.beust.jcommander.Parameter;

public class Parameters {

    private static Parameters instance;

    @Parameter(names = "--help", help = false, description = "Help")
    private boolean help;

    @Parameter(names = {"--suite", "-s"}, description = "Path to test suite")
    private String suite = "src\\main\\resources\\Api.xml";

    @Parameter(names = {"--logconfig", "-l"}, description = "Path to log4j.properties file")
    private String logConfig = "src\\main\\resources\\log4j.properties";

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public boolean isHelp() {
        return help;
    }

    public String getSuite() {
        return suite;
    }

    public String getLogConfig() {
        return logConfig;
    }
}

package api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import logger.Log;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CurrencyRequest {

    public double getCurrency(Date date) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        JsonNode tree;
        try {
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            String currencyUrl = "http://www.nbrb.by/API/ExRates/Rates/145?onDate=" + format1.format(date);
            Log.info("Get response for request " + currencyUrl);
            HttpGet httpGet = new HttpGet(currencyUrl);
            Log.info("Executing request " + httpGet.getRequestLine());
            CloseableHttpResponse rawResponse = httpClient.execute(httpGet);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
            tree = objectMapper.readTree(rawResponse.getEntity().getContent());
            Log.info("Currency rate " + date + " is " + tree.get("Cur_OfficialRate"));
        } finally {
            httpClient.close();
        }
        return Double.parseDouble(tree.get("Cur_OfficialRate").toString());
    }

    public double getResponse(ArrayList<Date> dates) throws IOException {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        double thursdayCurrency;
        double fridayCurrency;
        double confirmationsNumber = 0;
        Calendar calendar = Calendar.getInstance();
        for (int i = 0; i < dates.size(); i++) {
            calendar.setTime(dates.get(i));
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            thursdayCurrency = getCurrency(dates.get(i));
            fridayCurrency = getCurrency(calendar.getTime());
            if (thursdayCurrency < fridayCurrency) {
                Log.info("Currency rate is raised");
                confirmationsNumber++;
            }
        }
        return confirmationsNumber / (double) dates.size();
    }
}

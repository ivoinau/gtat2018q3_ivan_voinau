package api;

import logger.Log;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class WeatherRequest {

    public static final int NUMBER_OF_MONTH = 12;
    private static final int MEASUREMENTS_PER_DAY = 7;
    private static final int THURSDAY = 4;

    public void checkMinTempC(ArrayList<Date> datesList, Document document, XPath xPath)
            throws XPathExpressionException {
        Iterator<Date> dateIterator = datesList.iterator();
        while (dateIterator.hasNext()) {
            Date curDate = dateIterator.next();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            Log.info("Checking minimum temperature in " + format1.format(curDate));
            XPathExpression expression = xPath.compile("//date[contains(text(),'"
                    + format1.format(curDate) + "')]/parent::*/mintempC");
            Object res = expression.evaluate(document, XPathConstants.NODESET);
            NodeList nodeList = (NodeList) res;
            int temperature;
            for (int j = 0; j < nodeList.getLength(); j++) {
                temperature = Integer.parseInt(nodeList.item(j).getTextContent());
                if (temperature <= 0) {
                    dateIterator.remove();
                    Log.debug("Date " + format1.format(curDate) + " removed from list: Min temperature "
                            + temperature + " too low");
                }
            }
        }
    }

    public void checkPrecip(ArrayList<Date> datesList, Document document, XPath xPath) throws XPathExpressionException {
        Iterator<Date> dateIterator = datesList.iterator();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        while (dateIterator.hasNext()) {
            Date currentDate = dateIterator.next();
            Log.info("Check for " + format1.format(currentDate) + " precipitation");
            XPathExpression expression = xPath.compile("//date[contains(text(),'"
                    + format1.format(currentDate) + "')]/parent::*//precipMM");
            Object res = expression.evaluate(document, XPathConstants.NODESET);
            NodeList nodeList = (NodeList) res;
            double sum = 0;
            for (int j = 0; j < nodeList.getLength(); j++) {
                sum += Double.parseDouble(nodeList.item(j).getTextContent());
                if (j % MEASUREMENTS_PER_DAY == 0 && j != 0) {
                    if (sum == 0) {
                        dateIterator.remove();
                        Log.debug("Date " + format1.format(currentDate)
                                + " removed from list: there is no precipitation");
                    }
                    sum = 0;
                }
            }
        }
    }

    public ArrayList<Date> getDates(ArrayList<Date> dates, XPath xPath, ArrayList<Document> doc)
            throws XPathExpressionException, ParseException {
        Log.info("Get all Thursdays");
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        XPathExpression expression = xPath.compile("//date");
        for (int j = 0; j < NUMBER_OF_MONTH; j++) {
            Object res = expression.evaluate(doc.get(j), XPathConstants.NODESET);
            NodeList nodeList = (NodeList) res;
            for (int i = 0; i < nodeList.getLength(); i++) {
                String date = nodeList.item(i).getTextContent();
                Date thursdayDate = format1.parse(date);
                if (thursdayDate.getDay() == THURSDAY) {
                    dates.add(thursdayDate);
                    Log.debug("Find Thursday : " + thursdayDate);
                }
            }
        }
        return dates;
    }
}

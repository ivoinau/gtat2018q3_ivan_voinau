package bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties({"Date", "Cur_Scale"})
public class Currency {

    @JsonProperty("Cur_ID")
    private long id;
    @JsonProperty("Cur_Abbreviation")
    private String abbr;
    @JsonProperty("Cur_Name")
    private String curName;
    @JsonProperty("Cur_OfficialRate")
    private double rate;

    public double getRate() {
        return rate;
    }
}

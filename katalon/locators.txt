﻿Google search page :
query input
//input[@class='gLFyf gsfi']
.gLFyf

search in Google button
//div[@class="FPdoLc VlcLAe"]//input[@name="btnK"]
.FPdoLc input[name="btnK"]

I am lucky! button
//div[@class="FPdoLc VlcLAe"]//input[@name="btnI"]
.FPdoLc input[name="btnI"]

Search for nyan cat and create a locator that returns ALL result links (including references for video and references for resources where nyan cat was mentioned)
//div[@id="rso"]//a[not(@class)]
#rso a:not([class])

n-th o letter in Goooooooooogle   (n = 1...10)
//*[@id="nav"]//td[n+1]  where (n = 1...10)
#nav td:nth-child(n+1)  where (n = 1...10)

Mail.ru login page:
login input
//input[@id="mailbox:login"]
input[name="login"]

password input
//input[@id="mailbox:password"]
input[name="password"]

Enter button
//label[@id="mailbox:submit"]/input
input.o-control

Mail.ru main page (logged in):
links to folders

incoming
//a[@class="b-nav__link" and contains(@href,"inbox")]
a.b-nav__link[href$="inbox/"]

outcoming
//a[@class and contains(@href,"sent")]
a.b-nav__link[href$="sent/"]

spam
//a[@class and contains(@href,"spam")]
a.b-nav__link[href$="spam/"]

deleted
//a[@class and contains(@href,"trash")]
a.b-nav__link[href$="trash/"]

drafts
//a[@class and contains(@href,"drafts")]
a.b-nav__link[href$="drafts/"]

action buttons
write new letter
//div[@id="b-toolbar__left"]//a
div#b-toolbar__left a

delete
//*[@id="b-toolbar__right"]//div[@data-name = "remove"]
#b-toolbar__right div[data-name="remove"]

mark as spam
//*[@id="b-toolbar__right"]//div[@data-name = "spam"]
#b-toolbar__right div[data-name="spam"]

mark as not spam
//*[@id="b-toolbar__right"]//div[@data-name = "noSpam"]
#b-toolbar__right div[data-name="noSpam"]

mark as read
//div[@data-cache-key="0_undefined_false"]//a[contains(@data-subject,"VALUE")]//div[contains(@class,"ico_letterstatus_unread")]  where VALUE is part of subject of the letter
div[data-cache-key="0_undefined_false"] a[data-subject*="VALUE"] div[class*="ico_letterstatus_unread"]  where VALUE is part of subject of the letter


move to another folder
//*[@id="b-toolbar__right"]//div[contains(@class,"moveTo")]/div[@data-title]
#b-toolbar__right div[class*="moveTo"] div[data-title]

Checkbox for one exact letter
//*[@id="b-letters"]//a[contains(@data-subject,"VALUE")]//div[@class="b-checkbox__box"]  where VALUE is part of subject of the letter
#b-letters a[data-subject*="VALUE"] div.b-checkbox__box   where VALUE is part of subject of the letter

Opening link for one exact letter
//a[@class="js-href b-datalist__item__link" and contains(@data-title,"VALUE")] where VALUE is sender
a[class*="js-href b-datalist__item__link"][data-title*="VALUE"]  where VALUE is sender

New letter page
input for address
//textarea[@data-original-name="To"]
textarea[data-original-name="To"]

topic
//input[@name="Subject"]
input[name="Subject"]

text
//*[@id="tinymce"]
#tinymce

file attach
//input[@name="Filedata"]
input[name="Filedata"]
@disk
Feature: Yandex disk
  As Yandex disk user
  I want to reach any tab in disk
  and be able to create a folder

  Scenario: Switch to disk
    Given user is in home page
    When user clicks 'My services' button
    And user clicks 'Disc' button
    Then user is reaches disk

  Scenario: Switch to files
    Given user is in disk page
    When user clicks 'Files' button
    Then user is reached 'Files' tab

  Scenario: Switch to history
    Given user is in disk page
    When user clicks 'History' button
    Then user is reached 'History' tab

  Scenario: Switch to mail
    Given user is in disk page
    When user clicks 'Mail' button
    Then user is reached 'Mail' tab

  Scenario: Switch to photo
    Given user is in disk page
    When user clicks 'Photo' button
    Then user is reached 'Photo' tab

  Scenario: Switch to recent
    Given user is in disk page
    When user clicks 'Recent' button
    Then user is reached 'Recent' tab

  Scenario: Switch to shared
    Given user is in disk page
    When user clicks 'Shared' button
    Then user is reached 'Shared' tab

  Scenario: Switch to trash
    Given user is in disk page
    When user clicks 'Trash' button
    Then user is reached 'Trash' tab

  Scenario Outline: Create folder
    Given user is in disk page
    And user is in files tab
    When user clicks 'Create' button
    And user clicks 'Folder' button
    And user enter <name> name of folder
    And user clicks confirm button
    Then created folder <name> is displayed in 'files' tab

    Examples:
      |name         |
      |Folder123459 |
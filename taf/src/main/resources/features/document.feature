@document
Feature: Document in Yandex disk
  As Yandex disk user
  I want to create document
  and put some data in the document
  and this information should be saved

  Scenario: Create document
    Given user login successfully
    And user switched to Yandex disk
    And user switch to files tab
    And user creates folder if it not exist
    And user open folder
    When user clicks 'Create' button
    And user clicks 'Document' button
    Then user navigate to created document

  Scenario Outline: Check that text in document saved correctly
    Given user navigate to created document
    When user put <text> text in document
    And user closes document
    And user open document
    And user clicks 'Edit' button
    Then user see saved <text> in document

    Examples:
      |text      |
      |Hello     |

  Scenario: Check that user can delete document
    Given document is in folder
    When user delete document from folder
    And user switch to 'Trash' tab
    Then document is displayed in this tab

  Scenario: Check that document was deleted from folder
    Given user is in trash tab
    When user switch to files tab
    And user open folder
    Then user see that document does not exist in this folder
@login
Feature: Yandex login
  As Yandex disk user
  I want to reach files in disk after successful login

  Scenario Outline: Unsuccessful login
    Given user is on login form
    And user has no valid credentials
    When user enters <login> login
    And user enters <password> password
    Then user is stay in login form

    Examples:
    |login      |password   |
    |qwerty     |123456     |
    |TestLogin14|123qwe123  |
    |qwerty     |86a686aasdf|

  Scenario: Successful login
    Given user is on yandex login page
    And user has valid credentials
    |login      |password   |
    |TestLogin14|86a686aasdf|
    When user enters login
    And user enters password
    Then user is reaches his disk

package com.epam.gomel.tat.framework.config;

import org.openqa.selenium.WebDriver;

public class Configuration {

    public static final String LOGIN = "TestLogin14";
    public static final String PASSWORD = "86a686aasdf";
    public static final String WRONG_LOGIN = "qwerty";
    public static final String WRONG_PASS = "asdaffggqd";
    public static final String DISK_URL = "https://disk.yandex.ru/client/disk";
    public static final int TIMEOUT = 10;
    public static final int TIMEOUT_FOR_SAVE_DOC = 30;
    public static final int MAX_VALUE = 99999;
    public static final int ATTEMPTS = 10;
    public static WebDriver driver;
    static int random = (int) (Math.random() * MAX_VALUE);
    public static final String NAME = "Folder" + random;
}


package com.epam.gomel.tat.yandex.product.disk.services;

import com.epam.gomel.tat.framework.bo.Account;
import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import com.epam.gomel.tat.framework.config.Configuration;

public class AccountService extends Configuration {
    public void signIn(Account account) {
        LoginPage loginPage = new LoginPage();
        loginPage.enterLogin(account.getLogin());
        loginPage.enterPassword(account.getPassword());
    }
}

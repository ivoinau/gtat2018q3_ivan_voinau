package com.epam.gomel.tat.yandex.product.disk.screen;

import com.epam.gomel.tat.framework.config.Configuration;
import org.openqa.selenium.By;

import static com.epam.gomel.tat.framework.ui.browser.Browser.getInstance;

public class ServicesPage extends Configuration {
    private By myServicesButtonLocator = By.xpath("//a[@href=\"/profile/services\"]");
    private By discButtonlocator = By.xpath("//a[contains(@href,\"disk\")][@class=\"d-link d-link_black\"]");

    public ServicesPage clickMyServicesButton() {
        getInstance().click(myServicesButtonLocator);
        return this;
    }

    public ServicesPage clickDiscButton() {
        getInstance().click(discButtonlocator);
        return this;
    }

    public boolean isDiskPageOpened() {
        String url = getInstance().getUrl();
        return DISK_URL.equals(url);
    }
}

package com.epam.gomel.tat.yandex.product.disk.services;

import com.epam.gomel.tat.framework.bo.Folder;
import com.epam.gomel.tat.yandex.product.disk.screen.DiskPage;

public class FolderService {
    public void createFolder() {
        DiskPage diskPage = new DiskPage();
        diskPage.clickCreateButton();
        diskPage.clickCreateFolderButton();
    }

    public void typeFolderName(Folder folder) {
        DiskPage diskPage = new DiskPage();
        diskPage = diskPage.clearNameFolderInput();
        diskPage = diskPage.typeFolderName(folder.getName());
        diskPage.saveFolderName();
    }

    public void openFolder() {
        DiskPage diskPage = new DiskPage();
        diskPage.toFolder();
    }
}

package com.epam.gomel.tat.framework.test.yandex.product.steps;

import com.epam.gomel.tat.yandex.product.disk.screen.DiskPage;
import com.epam.gomel.tat.yandex.product.disk.services.FileService;
import com.epam.gomel.tat.yandex.product.disk.services.FolderService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class TrashStepDefs {

    @Given("user in trash tab")
    public void userInTrashTab() {
        DiskPage diskPage = new DiskPage();
        new FolderService().openFolder();
        FileService fileService = new FileService();
        fileService.createDocument();
        fileService.closeDocument();
        diskPage.docToTrash(DiskPage.docLocator);
        diskPage.toTrash();
    }

    @When("^user clicks 'Empty trash' button$")
    public void userClicksEmptyTrashButton() {
        new DiskPage().clickEmptyButton();
    }

    @And("^user clicks approve button$")
    public void userClicksApproveButton() {
        new DiskPage().clickApproveEmptyButton();
    }

    @Then("^trash tab does not contains files$")
    public void trashTabDoesNotContainsFiles() {
        boolean isDocumentInTrash = new DiskPage().isDocumentInTrash();
        Assert.assertFalse(isDocumentInTrash, "Document is not deleted from trashButtonLocator");
    }
}

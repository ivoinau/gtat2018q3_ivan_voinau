package com.epam.gomel.tat.yandex.product.disk.screen;

import com.epam.gomel.tat.framework.config.Configuration;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.gomel.tat.framework.ui.browser.Browser.getInstance;

public class DiskPage extends Configuration {

    public static By documentFrameLocator = By.xpath("//iframe[@class=\"editor-doc__iframe\"]");
    public static By recentButtonLocator = By
            .xpath("//div[contains(@class,\"sidebar_fixed\")]//a[@href=\"/client/recent\"]");
    public static By publishedButtonLocator = By
            .xpath("//a[contains(@class,\"navigation__link_current\") and @href=\"/client/published\"]");
    public static By sharedButtonLocator = By
            .xpath("//div[contains(@class,\"sidebar_fixed\")]//a[@href=\"/client/shared\"]");
    public static By photoButtonLocator = By
            .xpath("//div[contains(@class,\"sidebar_fixed\")]//a[@href=\"/client/photo\"]");
    public static By historyButtonLocator = By
            .xpath("//div[contains(@class,\"sidebar_fixed\")]//a[@href=\"/client/journal\"]");
    public static By mailButtonLocator = By
            .xpath("//div[contains(@class,\"sidebar_fixed\")]//a[@href=\"/client/mail\"]");
    public static By trashButtonLocator = By.xpath("//div[contains(@class,\"sidebar_fixed\")]//a[@id=\"/trash\"]");
    public static By filesButtonLocator = By
            .xpath("//div[contains(@class,\"sidebar_fixed\")]//a[@href=\"/client/disk\"]");
    public static By docLocator = By.xpath("//div[contains(@class,\"listing-item_type_file\")][1]");
    public static By folderIconLocator = By.xpath("//span[text()=\"" + NAME
            + "\" and @class=\"clamped-text\"]/parent::*/parent::*/parent::div");
    public static By emptyTrashButtonLocator = By
            .xpath("//button[contains(@class,\"client-listing__clean-trash-button\")]");
    public static By emptyApproveButtonLocator = By
            .xpath("//button[contains(@class,\"b-confirmation__action_right\")]");
    public static By createDocumentButtonLocator = By.xpath("//div[contains(@class,\"popup2_visible_yes\")]"
            + "//button/span[contains(@class,\"file-icon_doc\")]");
    private By createButtonLocator = By
            .xpath("//div[@class=\"root\"]//span[@class=\"create-resource-popup-with-anchor\"]/button");
    private By createFolderButtonLocator = By.xpath("//div[contains(@class,\"popup2_visible_yes\")]"
            + "//button/span[contains(@class,\"file-icon_dir_plus\")]");
    private By nameFolderInputLocator = By
            .xpath("//div[@class=\"modal__content\"]//input[@class=\"textinput__control\"]");
    private By saveButtonLocator = By
            .xpath("//div[@class=\"modal__content\"]//button[contains(@class,\"confirmation-dialog__button\")]");
    private By deleteButtonLocator = By
            .xpath("//button[contains(@class,\"groupable-buttons__visible-button_name_delete\")]");
    private By documentIconLocator = By
            .xpath("//span[contains(text(),\"Документ.\ndocx\")]/parent::*/parent::*/parent::div");
    private By inputLineLocator = By.xpath("//*[@id=\"WACViewPanel_EditingElement\"]/p/span/span");
    private By fileButtonLocator = By.xpath("//*[@id=\"jewelWordEditor-Default\"]/span");
    private By exitButtonLocator = By.xpath("//*[@id=\"btnjClose-Menu32\"]");
    private By saveStatusLocator = By.xpath("//*[@id=\"BreadcrumbSaveStatus\"]");
    private By folderLocator = By.xpath("//span[text()=\"" + NAME + "\" and @class=\"clamped-text\"]"
            + "/parent::*/parent::*/parent::div");
    private By editButtonlocator = By.xpath("//a[contains(@href,\"edit\")]");
    private By textLocator = By.xpath("//span[contains(text(),\"Hello\")]");

    public DiskPage toRecent() {
        getInstance().click(recentButtonLocator);
        return this;
    }

    public DiskPage toFiles() {
        getInstance().click(filesButtonLocator);
        return this;
    }

    public DiskPage toShared() {
        getInstance().click(sharedButtonLocator);
        return this;
    }

    public DiskPage toPhoto() {
        getInstance().click(photoButtonLocator);
        return this;
    }

    public DiskPage toHistory() {
        getInstance().click(historyButtonLocator);
        return this;
    }

    public DiskPage toMail() {
        getInstance().click(mailButtonLocator);
        return this;
    }

    public DiskPage toTrash() {
        getInstance().clickAction(trashButtonLocator);
        return this;
    }

    public DiskPage clickCreateButton() {
        getInstance().click(createButtonLocator);
        return this;
    }

    public DiskPage clickCreateFolderButton() {
        getInstance().click(createFolderButtonLocator);
        return this;
    }

    public DiskPage clearNameFolderInput() {
        getInstance().waitForPageLoaded();
        getInstance().clear(nameFolderInputLocator);
        return this;
    }

    public DiskPage typeFolderName(String folderName) {
        getInstance().type(nameFolderInputLocator, folderName);
        return this;
    }

    public DiskPage saveFolderName() {
        getInstance().click(saveButtonLocator);
        return this;
    }

    public DiskPage openStartPage() {
        getInstance().get("https://passport.yandex.ru");
        return this;
    }

    public DiskPage openDiskPage() {
        getInstance().get(DISK_URL);
        return this;
    }

    public DiskPage toFolder() {
        getInstance().waitForPageLoaded();
        getInstance().doubleClickAction(folderIconLocator);
        return this;
    }

    public DiskPage createDocument() {
        getInstance().click(createButtonLocator);
        getInstance().click(createDocumentButtonLocator);
        getInstance().switchToTab(1);
        return this;
    }

    public DiskPage switchToDocumentFrame() {
        getInstance().switchToFrame(documentFrameLocator);
        return this;
    }

    public DiskPage typeTextInDocument() {
        getInstance().clickAndTypeAction(inputLineLocator, "Hello World");
        return this;
    }

    public DiskPage typeTextInDocument(String text) {
        getInstance().clickAndTypeAction(inputLineLocator, text);
        return this;
    }

    public DiskPage clickDocumentExitButton() {
        getInstance().clickAction(fileButtonLocator);
        getInstance().clickAction(exitButtonLocator);
        return this;
    }

    public DiskPage waitForSaveText() {
        getInstance().checkElementText(saveStatusLocator, "Сохранение...");
        getInstance().checkElementText(saveStatusLocator, "Сохранено в Yandex");
        return this;
    }

    public DiskPage closeDocument() {
        getInstance().getWrappedDriver().close();
        getInstance().switchToTab(0);
        return this;
    }

    public DiskPage docToTrash(By locator) {
        int tries = 0;
        while (isDocumentInFolder() && tries < ATTEMPTS) {
            getInstance().clickAction(locator);
            getInstance().click(deleteButtonLocator);
            tries++;
        }
        toTrash();
        return this;
    }

    public DiskPage emptyTrash() {
        toTrash();
        getInstance().click(emptyTrashButtonLocator);
        getInstance().click(emptyApproveButtonLocator);
        return this;
    }

    public boolean isAppropriatePageDisplayed(By xpath) {
        try {
            (new WebDriverWait(getInstance().getWrappedDriver(), TIMEOUT))
                    .until(ExpectedConditions.presenceOfElementLocated(xpath));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean isFolderExist() {
        try {
            (new WebDriverWait(getInstance().getWrappedDriver(), TIMEOUT))
                    .until(ExpectedConditions.presenceOfElementLocated(folderLocator));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean isFolderExist(String name) {
        try {
            (new WebDriverWait(getInstance().getWrappedDriver(), TIMEOUT))
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()=\""
                            + name + "\" and @class=\"clamped-text\"]" + "/parent::*/parent::*/parent::div")));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean isDocumentInFolder() {
        try {
            (new WebDriverWait(getInstance().getWrappedDriver(), TIMEOUT))
                    .until(ExpectedConditions.presenceOfElementLocated(documentIconLocator));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public DiskPage openDocument() {
        getInstance().doubleClickAction(documentIconLocator);
        return this;
    }

    public DiskPage clickEditButton() {
        getInstance().click(editButtonlocator);
        return this;
    }

    public DiskPage switchToTab(int tab) {
        getInstance().switchToTab(tab);
        return this;
    }

    public DiskPage clickEmptyButton() {
        getInstance().click(emptyTrashButtonLocator);
        return this;
    }

    public DiskPage clickApproveEmptyButton() {
        getInstance().click(emptyApproveButtonLocator);
        return this;
    }

    public boolean isSavedTextPresent() {
        getInstance().switchToFrame(documentFrameLocator);
        try {
            (new WebDriverWait(getInstance().getWrappedDriver(), TIMEOUT))
                    .until(ExpectedConditions.presenceOfElementLocated(textLocator));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean isSavedTextPresent(String text) {
        getInstance().switchToFrame(documentFrameLocator);
        try {
            (new WebDriverWait(getInstance().getWrappedDriver(), TIMEOUT))
                    .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),\""
                            + text + "\")]")));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean isDocumentInTrash() {
        try {
            (new WebDriverWait(getInstance().getWrappedDriver(), TIMEOUT))
                    .until(ExpectedConditions.presenceOfElementLocated(documentIconLocator));
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }
}

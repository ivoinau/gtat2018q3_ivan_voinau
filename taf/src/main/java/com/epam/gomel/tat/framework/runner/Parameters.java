package com.epam.gomel.tat.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.gomel.tat.framework.ui.browser.BrowserType;

import java.net.MalformedURLException;
import java.net.URL;

import static com.epam.gomel.tat.framework.ui.browser.BrowserType.CHROME;

public class Parameters {

    private static Parameters instance;

    @Parameter(names = "--help", help = false, description = "Help")
    private boolean help;

    @Parameter(names = {"--browser", "-b"}, description = "Browser type",
            converter = Parameters.BrowserTypeConverter.class, required = true)
    private BrowserType browserType = CHROME;

    @Parameter(names = {"--gecko", "-g"}, description = "Path to GeckoDriver")
    private String geckoDriver = "src\\main\\resources\\geckodriver.exe";

    @Parameter(names = {"--chrome", "-c"}, description = "Path to ChromeDriver")
    private String chromeDriver = "src\\main\\resources\\chromedriver.exe";

    @Parameter(names = {"--suite", "-s"}, description = "Path to test suite")
    private String suite = "src\\main\\resources\\AllTests.xml";

    @Parameter(names = {"--logconfig", "-l"}, description = "Path to log4j.properties file")
    private String logConfig = "src\\main\\resources\\log4j.properties";

    @Parameter(names = {"--parallelMode", "-pm"}, description = "Mode for parallel run (tests or none)")
    private String parallelMode = "none";

    @Parameter(names = {"--thread", "-t"}, description = "Count of threads for parallel run")
    private int threadCount = 1;

    @Parameter(names = {"--selenium", "-sel"}, description = "Count of threads for parallel run")
    private URL seleniumHubURL;

    {
        try {
            seleniumHubURL = new URL("http://localhost:4445/wd/hub");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public boolean isHelp() {
        return help;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public String getChromeDriver() {
        return chromeDriver;
    }

    public String getGeckoDriver() {
        return geckoDriver;
    }

    public String getSuite() {
        return suite;
    }

    public String getLogConfig() {
        return logConfig;
    }

    public String getParallelMode() {
        return parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public URL getSeleniumHubURL() {
        return seleniumHubURL;
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType> {
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }
}

package com.epam.gomel.tat.framework.logger;

import com.epam.gomel.tat.framework.ui.browser.Browser;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

public class CustomAppender extends AppenderSkeleton {

    @Override
    protected void append(LoggingEvent loggingEvent) {

        String message = loggingEvent.getRenderedMessage();

        if (message.startsWith("Screenshot")) {
            message = String
                    .format("Screenshot taken: %s <a href='..logs/screenshots/%s' 'target=\"blank\">Screenshot</a>",
                            Browser.getInstance().getScreenshotName(), Browser.getInstance().getScreenshotName());
        }

        Reporter.log(String.format("%d %s %s %s,%s", System.currentTimeMillis(), loggingEvent.getLevel().toString(),
                loggingEvent.getLoggerName(), message, "<br>"), false);
    }

    @Override
    public void close() { }

    public boolean requiresLayout() {
        return true;
    }
}

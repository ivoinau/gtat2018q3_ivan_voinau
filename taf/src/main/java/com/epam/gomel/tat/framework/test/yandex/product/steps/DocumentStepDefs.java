package com.epam.gomel.tat.framework.test.yandex.product.steps;

import com.epam.gomel.tat.framework.bo.AccountFactory;
import com.epam.gomel.tat.framework.bo.FolderFactory;
import com.epam.gomel.tat.framework.logger.Log;
import com.epam.gomel.tat.yandex.product.disk.screen.DiskPage;
import com.epam.gomel.tat.yandex.product.disk.screen.ServicesPage;
import com.epam.gomel.tat.yandex.product.disk.services.AccountService;
import com.epam.gomel.tat.yandex.product.disk.services.FileService;
import com.epam.gomel.tat.yandex.product.disk.services.FolderService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import static com.epam.gomel.tat.framework.ui.browser.Browser.getInstance;

public class DocumentStepDefs {

    DiskPage diskPage;

    @When("^user open folder$")
    public void userOpenFolder() {
        new FolderService().openFolder();
    }

    @And("^user clicks 'Document' button$")
    public void userClicksDocumentButton() {
        getInstance().click(DiskPage.createDocumentButtonLocator);
        getInstance().switchToTab(1);
    }

    @Then("^user navigate to created document$")
    public void userNavigateToCreatedDocument() {
        Assert.assertTrue(new FileService().isUserSwitchedToDocument(), "Document has been not created");
    }

    @When("^user put (.*) text in document$")
    public void userPutSomeDataInDocument(String text) {
        diskPage = new DiskPage().typeTextInDocument(text);
    }

    @And("^user closes document$")
    public void userClosesDocument() {
        diskPage.clickDocumentExitButton();
        diskPage = diskPage.waitForSaveText();
        diskPage.closeDocument();
    }

    @And("^user open document$")
    public void userOpenDocument() {
        diskPage.openDocument();
        diskPage.switchToTab(1);
    }

    @And("^user clicks 'Edit' button$")
    public void userClicksEditButton() {
        diskPage.clickEditButton();
    }

    @Then("^user see saved (.*) in document$")
    public void userSeeSavedInfoInDocument(String text) {
        boolean isTextCorrect = diskPage.isSavedTextPresent(text);
        diskPage.switchToTab(0);
        Assert.assertTrue(isTextCorrect, "Text in document saved incorrectly");
    }

    @Given("^document is in folder$")
    public void documentIsInFolder() {
        diskPage = new DiskPage();
        Assert.assertTrue(diskPage.isDocumentInFolder(), "Document is not exist");
    }

    @When("^user delete document from folder$")
    public void userDeleteDocumentFromFolder() {
        diskPage.docToTrash(DiskPage.docLocator);
    }

    @And("^user switch to 'Trash' tab$")
    public void userSwitchToTrashTab() {
        diskPage.toTrash();
    }

    @Then("^document is displayed in this tab$")
    public void documentIsDisplayedInThisTab() {
        boolean isDocumentInTrash = diskPage.isDocumentInTrash();
        Assert.assertTrue(isDocumentInTrash, "Document has been not deleted");
    }

    @Given("^user is in trash tab$")
    public void userIsInTrashTab() {
        new DiskPage().toTrash();
    }

    @When("^user switch to files tab$")
    public void userSwitchToFilesTab() {
        diskPage = new DiskPage().toFiles();
    }

    @Then("^user see that document does not exist in this folder$")
    public void userSeeThatDocumentDoesNotExistInThisFolder() {
        boolean isDocumentInFolder = diskPage.isDocumentInFolder();
        Assert.assertFalse(isDocumentInFolder, "Document has been not deleted from folder");
    }

    @Given("^user login successfully$")
    public void userLoginSuccessfully() {
        diskPage = new DiskPage();
        diskPage.openStartPage();
        new AccountService().signIn(AccountFactory.defaultAccount());
    }

    @And("^user switched to Yandex disk$")
    public void userSwitchToYandexDisk() {
        ServicesPage servicesPage = new ServicesPage();
        servicesPage.clickMyServicesButton();
        servicesPage.clickDiscButton();
    }

    @And("^user creates folder if it not exist$")
    public void userCreatesFolderIfItNotExist() {
        if (!diskPage.isFolderExist()) {
            Log.debug("Folder is not exist");
            FolderService folderService = new FolderService();
            folderService.createFolder();
            folderService.typeFolderName(FolderFactory.defaultFolder());
        }
    }
}

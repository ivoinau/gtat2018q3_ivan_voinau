package com.epam.gomel.tat.framework.listener;

import com.epam.gomel.tat.framework.ui.browser.Browser;
import org.testng.*;

import static com.epam.gomel.tat.framework.logger.Log.logger;

public class Listener implements IInvokedMethodListener, ISuiteListener, IClassListener {
    private static final int SUCCESS = 1;
    private static final int FAILED = 2;
    private static final int SKIPPED = 3;

    public void onStart(ISuite iSuite) {
        logger.info("[SUITE STARTED] " + iSuite.getName());
    }

    public void onFinish(ISuite iSuite) {
        logger.info("[SUITE FINISHED] " + iSuite.getName());
    }

    public void onBeforeClass(ITestClass iTestClass) {
        logger.info("[CLASS STARTED] - " + iTestClass.getName());
    }

    public void onAfterClass(ITestClass iTestClass) {
        logger.info("[BROWSER CLOSED] - " + iTestClass.getName());
        Browser.getInstance().closeBrowser();
    }

    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        String methodName = iInvokedMethod.getTestMethod().getTestClass().getName()
                + "." + iInvokedMethod.getTestMethod().getMethodName();
        logger.debug("[TEST STARTED]  - " + methodName);
    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        String methodName = iInvokedMethod.getTestMethod().getTestClass().getName()
                + "." + iInvokedMethod.getTestMethod().getMethodName();
        switch (iTestResult.getStatus()) {
            case SUCCESS:
                logger.debug("[TEST SUCCESS] - " + methodName);
                break;

            case FAILED:
                logger.debug("[TEST FAILED] - " + methodName);
                break;

            case SKIPPED:
                logger.debug("[TEST SKIPPED] - " + methodName);
                break;
            default:
                logger.error("[TEST UNKNOWABLE STATUS] - " + methodName);
        }
    }
}

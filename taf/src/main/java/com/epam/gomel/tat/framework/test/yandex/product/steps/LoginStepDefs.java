package com.epam.gomel.tat.framework.test.yandex.product.steps;

import com.epam.gomel.tat.framework.bo.Account;
import com.epam.gomel.tat.framework.logger.Log;
import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import java.util.List;

public class LoginStepDefs {

    private Account account;
    private LoginPage loginPage;

    @Given("^user is on (?:login form|yandex login page)$")
    public void userIsOnLoginForm() {
        loginPage = new LoginPage();
        loginPage.open();
    }

    @And("^user has valid credentials$")
    public void userHasValidCredentials(List<Account> accounts) {
        for (Account acc : accounts) {
            this.account = acc;
            Log.info("created account:" + account);
        }
    }

    @When("^user enters (.*) login$")
    public void userEntersParamLogin(String login) {
        loginPage.enterLogin(login);
    }

    @When("^user enters login$")
    public void userEntersLogin() {
        loginPage.enterLogin(account.getLogin());
    }

    @And("^user enters (.*) password$")
    public void userEntersParamPassword(String password) {
        loginPage.enterPassword(password);
    }

    @And("^user enters password$")
    public void userEntersThePassword() {
        loginPage.enterPassword(account.getPassword());
    }

    @Then("^user is reaches his disk$")
    public void userIsReachesHisDisk() {
        boolean isLogInSuccess = loginPage.isLogInSuccess();
        Assert.assertTrue(isLogInSuccess, "Can't login with correct data");
    }

    @And("^user has no valid credentials$")
    public void userHasInvalidCredentials() {
        account = new Account();
    }

    @Then("^user is stay in login form$")
    public void userIsStayInLoginForm() {
        boolean isLogInSuccess = loginPage.isLogInSuccess();
        Assert.assertFalse(isLogInSuccess, "log in successfully with incorrect data");
    }
}

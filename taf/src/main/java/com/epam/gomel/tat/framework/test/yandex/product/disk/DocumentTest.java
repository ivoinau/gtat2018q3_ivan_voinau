package com.epam.gomel.tat.framework.test.yandex.product.disk;

import com.epam.gomel.tat.framework.bo.AccountFactory;
import com.epam.gomel.tat.framework.bo.FolderFactory;
import com.epam.gomel.tat.framework.logger.Log;
import com.epam.gomel.tat.yandex.product.disk.screen.DiskPage;
import com.epam.gomel.tat.yandex.product.disk.screen.ServicesPage;
import com.epam.gomel.tat.yandex.product.disk.services.AccountService;
import com.epam.gomel.tat.yandex.product.disk.services.FileService;
import com.epam.gomel.tat.yandex.product.disk.services.FolderService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DocumentTest {
    DiskPage diskPage;

    @BeforeClass
    public void openStartPage() {
        diskPage = new DiskPage();
        diskPage.openStartPage();
        new AccountService().signIn(AccountFactory.defaultAccount());
        ServicesPage servicesPage = new ServicesPage();
        servicesPage.clickMyServicesButton();
        servicesPage.clickDiscButton();
        diskPage.toFiles();
        if (!diskPage.isFolderExist()) {
            Log.debug("Folder is not exist");
            FolderService folderService = new FolderService();
            folderService.createFolder();
            folderService.typeFolderName(FolderFactory.defaultFolder());
        }
    }

    @Test(description = "Test that user can create document in folder created before")
    public void createDocument() {
        new FolderService().openFolder();
        FileService fileService = new FileService();
        fileService.createDocument();
        Assert.assertTrue(fileService.isUserSwitchedToDocument(), "Document has been not created");
    }

    @Test(dependsOnMethods = "createDocument",
            description = "Test that user can write text to document "
                    + "and check that text saves correctly")
    public void writeToDocument() {
        new FileService().writeToDoc();
        boolean isDocumentCreated = diskPage.isDocumentInFolder();
        Assert.assertTrue(isDocumentCreated, "Document is not exist in folder");
        diskPage.openDocument();
        diskPage.switchToTab(1);
        diskPage.clickEditButton();
        boolean isTextCorrect = diskPage.isSavedTextPresent();
        diskPage.switchToTab(0);
        Assert.assertTrue(isTextCorrect, "Text in document saved incorrectly");
    }

    @Test(dependsOnMethods = "writeToDocument",
            description = "Test that user can delete created document")
    public void documentInTrash() {
        diskPage.docToTrash(DiskPage.docLocator);
        boolean isDocumentInTrash = diskPage.isDocumentInTrash();
        Assert.assertTrue(isDocumentInTrash, "Document has been not deleted");
    }

    @Test(dependsOnMethods = "documentInTrash")
    public void deleteDocumentFromFolder() {
        diskPage.toFiles();
        new FolderService().openFolder();
        boolean isDocumentInFolder = diskPage.isDocumentInFolder();
        Assert.assertFalse(isDocumentInFolder, "Document has been not deleted from folder");
    }

}

package com.epam.gomel.tat.framework.bo;

import static com.epam.gomel.tat.framework.config.Configuration.*;

public class AccountFactory {
    public static Account defaultAccount() {
        Account account = new Account();
        account.setLogin(LOGIN);
        account.setPassword(PASSWORD);
        return account;
    }

    public static Account incorrectAccount() {
        Account account = new Account();
        account.setLogin(WRONG_LOGIN);
        account.setPassword(WRONG_PASS);
        return account;
    }
}

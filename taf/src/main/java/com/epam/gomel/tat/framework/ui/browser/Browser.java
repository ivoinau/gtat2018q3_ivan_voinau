package com.epam.gomel.tat.framework.ui.browser;

import com.epam.gomel.tat.framework.logger.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.epam.gomel.tat.framework.config.Configuration.*;

public final class Browser {

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();
    private String screenshotName;
    private WebDriver wrappedWebDriver;

    private Browser() {
        wrappedWebDriver = BrowserFactory.getBrowser();
    }

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public String getScreenshotName() {
        return screenshotName;
    }

    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public void closeBrowser() {
        try {
            if (getWrappedDriver() != null) {
                getWrappedDriver().quit();
            }
        } catch (WebDriverException e) {
            Log.error(e.getMessage());
        } finally {
            instance.set(null);
        }
    }

    public void get(String url) {
        Log.info("WebDriver navigated to url: " + url);
        wrappedWebDriver.get(url);
        makeScreenshot();
    }

    public void click(By by) {
        Log.info("Click " + by);
        waitForElementClickable(by).click();
    }

    public void clickAction(By by) {
        Log.info("Click " + by);
        waitForPageLoaded();
        WebElement element = waitForElementClickable(by);
        highlightBackground(element);
        Actions actions = new Actions(wrappedWebDriver);
        actions.moveToElement(element);
        actions.click();
        actions.build().perform();
    }

    public void clickAndTypeAction(By by, String text) {
        Log.info("Click " + by);
        waitForPageLoaded();
        WebElement element = waitForElementClickable(by);
        waitForReadyState();
        highlightBackground(element);
        Log.info("Type " + text + " in" + by);
        element = wrappedWebDriver.findElement(by);
        Actions actions = new Actions(wrappedWebDriver);
        actions.moveToElement(element);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
    }

    public void doubleClickAction(By by) {
        Log.info("Double click " + by);
        WebElement element = waitForElementClickable(by);
        highlightBackground(element);
        Actions actions = new Actions(wrappedWebDriver);
        actions.moveToElement(element);
        actions.doubleClick();
        actions.build().perform();
    }

    public void switchToTab(int tab) {
        ArrayList<String> tabs = new ArrayList<String>(wrappedWebDriver.getWindowHandles());
        wrappedWebDriver.switchTo().window(tabs.get(tab));
    }

    public void type(By by, String keys) {
        Log.info("type " + keys + " in textfield " + by);
        highlightBackground(waitForElementClickable(by));
        wrappedWebDriver.findElement(by).sendKeys(keys);
    }

    public void clear(By by) {
        wrappedWebDriver.findElement(by).sendKeys(Keys.chord(Keys.LEFT_CONTROL, "a"));
        Log.info("Element " + by + "cleared");
    }

    public void checkElementText(By by, String text) {
        Log.debug("Element " + by + "has text " + text);
        (new WebDriverWait(wrappedWebDriver, TIMEOUT_FOR_SAVE_DOC)).until(ExpectedConditions.textToBe(by, text));
    }

    public String getUrl() {
        return Browser.getInstance().getWrappedDriver().getCurrentUrl();
    }

    public void refresh() {
        Browser.getInstance().getWrappedDriver().navigate().refresh();
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = driver ->
                ((JavascriptExecutor) driver).executeScript("return jQuery.active == 0").toString()
                        .equalsIgnoreCase("true");
    }

    public void waitForReadyState() {
        ExpectedCondition<Boolean> expectation = driver ->
                ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
                        .equalsIgnoreCase("complete");
    }

    public void switchToFrame(By by) {
        if (by == null) {
            Log.debug("Switch to default frame ");
            wrappedWebDriver.switchTo().defaultContent();
        } else {
            Log.debug("Switch to frame " + by);
            (new WebDriverWait(wrappedWebDriver, TIMEOUT))
                    .until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(by));
        }
    }

    public WebElement waitForElementClickable(By by) {
        return (new WebDriverWait(wrappedWebDriver, TIMEOUT)).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(by));
    }

    public WebElement waitForPresenceOfElementLocated(By by) {
        return (new WebDriverWait(wrappedWebDriver, TIMEOUT)).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public void makeScreenshot() {
        screenshotName = System.nanoTime() + "screenshot.png";
        File screenshot = new File("logs/screenshots/" + screenshotName);
        try {
            File screenshotAsFile = ((TakesScreenshot) wrappedWebDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshotAsFile, screenshot);
            Log.info("Screenshot taken: file " + screenshot.getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException("Failed screenshot ", e);
        }
    }

    public void highlightBackground(WebElement element) {
        String backgroundColor = element.getCssValue("backgroundColor");
        JavascriptExecutor js = (JavascriptExecutor) wrappedWebDriver;
        js.executeScript("arguments[0].style.backgroundColor = '" + "yellow" + "'", element);
        makeScreenshot();
        js.executeScript("arguments[0].style.backgroundColor = '" + backgroundColor + "'", element);
    }
}

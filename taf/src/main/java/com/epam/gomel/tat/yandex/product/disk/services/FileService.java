package com.epam.gomel.tat.yandex.product.disk.services;

import com.epam.gomel.tat.yandex.product.disk.screen.DiskPage;
import org.openqa.selenium.TimeoutException;

public class FileService {
    public void createDocument() {
        DiskPage diskPage = new DiskPage();
        diskPage.createDocument();
    }

    public void writeToDoc() {
        DiskPage diskPage = new DiskPage();
        diskPage = diskPage.typeTextInDocument();
        diskPage = diskPage.clickDocumentExitButton();
        diskPage = diskPage.waitForSaveText();
        diskPage.closeDocument();
    }

    public void closeDocument() {
        DiskPage diskPage = new DiskPage();
        diskPage = diskPage.switchToDocumentFrame();
        diskPage.closeDocument();
    }

    public boolean isUserSwitchedToDocument() {
        DiskPage diskPage = new DiskPage();
        try {
            diskPage.switchToDocumentFrame();
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }
}

package com.epam.gomel.tat.framework.ui.browser;

public enum BrowserType {
    CHROME, FIREFOX;
}

package com.epam.gomel.tat.yandex.product.disk.screen;

import com.epam.gomel.tat.framework.config.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;

import static com.epam.gomel.tat.framework.ui.browser.Browser.getInstance;

public class LoginPage extends Configuration {

    //New form - form where only login field is displayed after open start page
    private By inputLoginNewFormLocator = By.id("passp-field-login");
    //Old form - form where login and password fields are displayed after open start page
    private By inputLoginOldFormLocator = By.xpath("//input[@name=\"login\"]");
    private By confirmLoginButtonLocator = By
            .xpath("//*[@id=\"root\"]//button[contains(@class,\"button2_theme_action\")]");
    private By inputPassNewFormLocator = By.id("passp-field-passwd");
    private By inputPassOldFormLocator = By.xpath("//input[@name=\"passwd\"]");
    private By signInOldFormButtonLocator = By.xpath("//span[@class=\"passport-Button-Text\"]");
    private By signInNewFormButtonLocator = By.xpath("//*[@id=\"root\"]//button");
    private By userInfoLocator = By.xpath("//*[contains(@data-login,\"TestLogin14\")]");
    private WebElement inputLogin;
    private WebElement inputPass;

    public LoginPage enterLogin(String login) {
        try {
            inputLogin = getInstance().waitForElementClickable(inputLoginNewFormLocator);
        } catch (TimeoutException e) {
            getInstance().type(inputLoginOldFormLocator, login);
        }
        if (inputLogin != null) {
            getInstance().type(inputLoginNewFormLocator, login);
            getInstance().click(confirmLoginButtonLocator);
        }
        return this;
    }

    public LoginPage enterPassword(String password) {
        try {
            inputPass = getInstance().waitForElementClickable(inputPassNewFormLocator);
        } catch (TimeoutException e) {
            getInstance().type(inputPassOldFormLocator, password);
            getInstance().click(signInOldFormButtonLocator);
        }
        if (inputPass != null) {
            getInstance().type(inputPassNewFormLocator, password);
            getInstance().click(signInNewFormButtonLocator);
        }
        return this;
    }

    public boolean isLogInSuccess() {
        try {
            getInstance().waitForPresenceOfElementLocated(userInfoLocator);
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public LoginPage open() {
        getInstance().get("https://passport.yandex.ru");
        return this;
    }
}

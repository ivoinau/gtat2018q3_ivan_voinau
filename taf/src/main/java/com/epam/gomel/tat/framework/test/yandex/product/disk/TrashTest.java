package com.epam.gomel.tat.framework.test.yandex.product.disk;

import com.epam.gomel.tat.framework.bo.AccountFactory;
import com.epam.gomel.tat.framework.bo.FolderFactory;
import com.epam.gomel.tat.yandex.product.disk.screen.DiskPage;
import com.epam.gomel.tat.yandex.product.disk.screen.ServicesPage;
import com.epam.gomel.tat.yandex.product.disk.services.AccountService;
import com.epam.gomel.tat.yandex.product.disk.services.FileService;
import com.epam.gomel.tat.yandex.product.disk.services.FolderService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TrashTest {

    DiskPage diskPage;

    @BeforeClass(description = "Opening start page, sign in, create empty document and put it to trash")
    public void openStartPage() {
        diskPage = new DiskPage();
        diskPage.openStartPage();
        new AccountService().signIn(AccountFactory.defaultAccount());
        ServicesPage servicesPage = new ServicesPage();
        servicesPage.clickMyServicesButton();
        servicesPage.clickDiscButton();
        diskPage.toFiles();
        if (!diskPage.isFolderExist()) {
            FolderService folderService = new FolderService();
            folderService.createFolder();
            folderService.typeFolderName(FolderFactory.defaultFolder());
        }
        new FolderService().openFolder();
        FileService fileService = new FileService();
        fileService.createDocument();
        fileService.closeDocument();
        diskPage.docToTrash(DiskPage.docLocator);
    }

    @Test(description = "Test that user can delete all filesButtonLocator in trashButtonLocator")
    public void emptyTrash() {
        diskPage.toTrash();
        diskPage.emptyTrash();
        boolean isDocumentInTrash = diskPage.isDocumentInTrash();
        Assert.assertFalse(isDocumentInTrash, "Document is not deleted from trashButtonLocator");
    }
}

package com.epam.gomel.tat.framework.test.yandex.product.steps;

import com.epam.gomel.tat.framework.bo.AccountFactory;
import com.epam.gomel.tat.yandex.product.disk.screen.DiskPage;
import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import com.epam.gomel.tat.yandex.product.disk.screen.ServicesPage;
import com.epam.gomel.tat.yandex.product.disk.services.AccountService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class DiskStepDefs {

    ServicesPage servicesPage;

    @Given("^user is in home page$")
    public void userIsOnLoginForm() {
        new LoginPage().open();
        new AccountService().signIn(AccountFactory.defaultAccount());
    }

    @When("^user clicks 'My services' button$")
    public void userClicksMyServicesButton() {
        servicesPage = new ServicesPage();
        servicesPage.clickMyServicesButton();
    }

    @And("^user clicks 'Disc' button$")
    public void userClicksDiscButton() {
        servicesPage = new ServicesPage();
        servicesPage.clickDiscButton();
    }

    @Then("^user is reaches disk$")
    public void userIsReachesDisk() {
        Assert.assertTrue(new ServicesPage().isDiskPageOpened(), "Current page is not yandex disk");
    }

    @When("^user clicks 'Files' button$")
    public void userClicksFilesButton() {
        new DiskPage().toFiles();
    }

    @Then("^user is reached 'Files' tab$")
    public void userIsReachedFilesTab() {
        boolean isFilesPageDisplayed = new DiskPage().isAppropriatePageDisplayed(DiskPage.filesButtonLocator);
        Assert.assertTrue(isFilesPageDisplayed, "'files' page does not displayed");
    }

    @When("^user clicks 'History' button$")
    public void userClicksHistoryButton() {
        new DiskPage().toHistory();
    }

    @Then("^user is reached 'History' tab$")
    public void userIsReachedHistoryTab() {
        boolean isHistoryPageDisplayed = new DiskPage().isAppropriatePageDisplayed(DiskPage.historyButtonLocator);
        Assert.assertTrue(isHistoryPageDisplayed, "'history' page does not displayed");
    }

    @When("^user clicks 'Mail' button$")
    public void userClicksMailButton() {
        new DiskPage().toMail();
    }

    @Then("^user is reached 'Mail' tab$")
    public void userIsReachedMailTab() {
        boolean isMailPageDisplayed = new DiskPage().isAppropriatePageDisplayed(DiskPage.mailButtonLocator);
        Assert.assertTrue(isMailPageDisplayed, "'mail' page does not displayed");
    }

    @When("^user clicks 'Photo' button$")
    public void userClicksPhotoButton() {
        new DiskPage().toPhoto();
    }

    @Then("^user is reached 'Photo' tab$")
    public void userIsReachedPhotoTab() {
        boolean isPhotoPageDisplayed = new DiskPage().isAppropriatePageDisplayed(DiskPage.photoButtonLocator);
        Assert.assertTrue(isPhotoPageDisplayed, "'photo' page does not displayed");
    }

    @When("^user clicks 'Recent' button$")
    public void userClicksRecentButton() {
        new DiskPage().toRecent();
    }

    @Then("^user is reached 'Recent' tab$")
    public void userIsReachedRecentTab() {
        boolean isRecentPageDisplayed = new DiskPage().isAppropriatePageDisplayed(DiskPage.recentButtonLocator);
        Assert.assertTrue(isRecentPageDisplayed, "'recent' page does not displayed");
    }

    @When("^user clicks 'Shared' button$")
    public void userClicksSharedButton() {
        new DiskPage().toShared();
    }

    @Then("^user is reached 'Shared' tab$")
    public void userIsReachedSharedTab() {
        boolean isSharedPageDisplayed = new DiskPage().isAppropriatePageDisplayed(DiskPage.publishedButtonLocator);
        Assert.assertTrue(isSharedPageDisplayed, "'shared' page does not displayed");
    }

    @When("^user clicks 'Trash' button$")
    public void userClicksTrashButton() {
        new DiskPage().toTrash();
    }

    @Then("^user is reached 'Trash' tab$")
    public void userIsReachedTrashTab() {
        boolean isTrashPageDisplayed = new DiskPage().isAppropriatePageDisplayed(DiskPage.trashButtonLocator);
        Assert.assertTrue(isTrashPageDisplayed, "'trash' page does not displayed");
    }

    @And("^user is in files tab$")
    public void userIsInFilesTab() {
        new DiskPage().toFiles();
    }

    @When("^user clicks 'Create' button$")
    public void userClicksCreateButton() {
        new DiskPage().clickCreateButton();
    }

    @And("^user clicks 'Folder' button$")
    public void userClicksFolderButton() {
        new DiskPage().clickCreateFolderButton();
    }

    @And("^user enter (.*) name of folder$")
    public void userEnterNameOfFolder(String name) {
        DiskPage diskPage = new DiskPage().clearNameFolderInput();
        diskPage.typeFolderName(name);
    }

    @And("^user clicks confirm button$")
    public void userClicksConfirmButton() {
        new DiskPage().saveFolderName();
    }

    @Then("^created folder (.*) is displayed in 'files' tab$")
    public void createdFolderIsDisplayedInFilesTab(String name) {
        boolean isFolderExist = new DiskPage().isFolderExist(name);
        Assert.assertTrue(isFolderExist, "folder does not exist");
    }

    @Given("^user is in disk page$")
    public void userIsInDiskPage() {
        new DiskPage().openDiskPage();
    }
}

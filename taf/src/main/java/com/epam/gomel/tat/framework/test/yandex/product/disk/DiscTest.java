package com.epam.gomel.tat.framework.test.yandex.product.disk;

import com.epam.gomel.tat.framework.bo.AccountFactory;
import com.epam.gomel.tat.framework.bo.FolderFactory;
import com.epam.gomel.tat.yandex.product.disk.screen.DiskPage;
import com.epam.gomel.tat.yandex.product.disk.screen.ServicesPage;
import com.epam.gomel.tat.yandex.product.disk.services.AccountService;
import com.epam.gomel.tat.yandex.product.disk.services.FolderService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DiscTest {

    private DiskPage diskPage;

    @BeforeClass(description = "Opening start page and sign in")
    public void openStartPage() {
        diskPage = new DiskPage();
        diskPage.openStartPage();
        new AccountService().signIn(AccountFactory.defaultAccount());
    }

    @Test(description = "Test that user can switch to yandex disk")
    public void switchToDisk() {
        ServicesPage servicesPage = new ServicesPage();
        servicesPage.clickMyServicesButton();
        servicesPage.clickDiscButton();
        Assert.assertTrue(servicesPage.isDiskPageOpened(), "Current page is not yandex disk");
    }

    @Test(dependsOnMethods = "switchToDisk",
            description = "Test that user can switch to 'Files' menu")
    public void toFilesTest() {
        diskPage.toFiles();
        boolean isFilesPageDisplayed = diskPage.isAppropriatePageDisplayed(DiskPage.filesButtonLocator);
        Assert.assertTrue(isFilesPageDisplayed, "'files' page does not displayed");
    }

    @Test(dependsOnMethods = "switchToDisk",
            description = "Test that user can switch to 'History' menu")
    public void toHistoryTest() {
        diskPage.toHistory();
        boolean isHistoryPageDisplayed = diskPage.isAppropriatePageDisplayed(DiskPage.historyButtonLocator);
        Assert.assertTrue(isHistoryPageDisplayed, "'history' page does not displayed");
    }

    @Test(dependsOnMethods = "switchToDisk",
            description = "Test that user can switch to 'Mail' menu")
    public void toMailTest() {
        diskPage.toMail();
        boolean isMailPageDisplayed = diskPage.isAppropriatePageDisplayed(DiskPage.mailButtonLocator);
        Assert.assertTrue(isMailPageDisplayed, "'mail' page does not displayed");
    }

    @Test(dependsOnMethods = "switchToDisk",
            description = "Test that user can switch to 'Photo' menu")
    public void toPhotoTest() {
        diskPage.toPhoto();
        boolean isPhotoPageDisplayed = diskPage.isAppropriatePageDisplayed(DiskPage.photoButtonLocator);
        Assert.assertTrue(isPhotoPageDisplayed, "'photo' page does not displayed");
    }

    @Test(dependsOnMethods = "switchToDisk",
            description = "Test that user can switch to 'Recent' menu")
    public void toRecentTest() {
        diskPage.toRecent();
        boolean isRecentPageDisplayed = diskPage.isAppropriatePageDisplayed(DiskPage.recentButtonLocator);
        Assert.assertTrue(isRecentPageDisplayed, "'recent' page does not displayed");
    }

    @Test(dependsOnMethods = "switchToDisk",
            description = "Test that user can switch to 'Shared' menu")
    public void toSharedTest() {
        diskPage.toShared();
        boolean isSharedPageDisplayed = diskPage.isAppropriatePageDisplayed(DiskPage.publishedButtonLocator);
        Assert.assertTrue(isSharedPageDisplayed, "'shared' page does not displayed");
    }

    @Test(dependsOnMethods = "switchToDisk",
            description = "Test that user can switch to 'Trash' menu")
    public void toTrashTest() {
        diskPage.toTrash();
        boolean isTrashPageDisplayed = diskPage.isAppropriatePageDisplayed(DiskPage.trashButtonLocator);
        Assert.assertTrue(isTrashPageDisplayed, "'trash' page does not displayed");
    }

    @Test(dependsOnMethods = "switchToDisk", priority = 1,
            description = "Test that user can create new folder in disk")
    public void createFolderTest() {
        diskPage.toFiles();
        FolderService folderService = new FolderService();
        folderService.createFolder();
        folderService.typeFolderName(FolderFactory.defaultFolder());
        boolean isFolderExist = diskPage.isFolderExist();
        Assert.assertTrue(isFolderExist, "folder does not exist");
    }
}

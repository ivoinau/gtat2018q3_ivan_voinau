package com.epam.gomel.tat.framework.ui.browser;

import org.openqa.selenium.WebDriver;

import com.epam.gomel.tat.framework.runner.Parameters;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.concurrent.TimeUnit;

public final class BrowserFactory {

    private BrowserFactory() {
    }

    public static WebDriver getBrowser() {
        WebDriver webDriver;
        switch (Parameters.instance().getBrowserType()) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", Parameters.instance().getChromeDriver());
                DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
                webDriver = new RemoteWebDriver(Parameters.instance().getSeleniumHubURL(), desiredCapabilities);
                break;
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", Parameters.instance().getGeckoDriver());
                DesiredCapabilities desiredCapabilities1 = DesiredCapabilities.firefox();
                webDriver = new RemoteWebDriver(Parameters.instance().getSeleniumHubURL(), desiredCapabilities1);
                break;
            default:
                throw new RuntimeException("Not supported browser "
                        + Parameters.instance().getBrowserType());
        }
        configure(webDriver);
        return webDriver;
    }

    private static void configure(WebDriver webDriver) {
        webDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }
}

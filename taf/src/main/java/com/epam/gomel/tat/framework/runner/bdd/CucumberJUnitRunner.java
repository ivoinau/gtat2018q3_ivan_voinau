package com.epam.gomel.tat.framework.runner.bdd;

import com.epam.gomel.tat.framework.logger.Log;
import com.epam.gomel.tat.framework.ui.browser.Browser;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features = "src/main/resources/features",
        glue = "com.epam.gomel.tat.framework.test.yandex.product.steps",
        tags = "@login",
        plugin = {
                "pretty", "json:target/Cucumber.json",
                "html:target/cucumber-html-report"
        }
)

public class CucumberJUnitRunner {

    @AfterClass
    public static void tearDown() {
        Log.info("Browser will be closed!");
        Browser.getInstance().closeBrowser();
    }
}

package com.epam.gomel.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.gomel.tat.framework.listener.Listener;
import com.epam.gomel.tat.framework.logger.Log;
import org.apache.log4j.PropertyConfigurator;
import org.testng.TestNG;

import java.util.Collections;

import static com.epam.gomel.tat.framework.runner.Parameters.instance;

public class TestRunner {

    public static TestNG createTestNG() {
        TestNG testNG = new TestNG();
        testNG.addListener(new Listener());
        testNG.setTestSuites(Collections.singletonList(instance().getSuite()));
        testNG.setParallel(instance().getParallelMode());
        testNG.setThreadCount(instance().getThreadCount());
        return testNG;
    }

    public static void main(String[] args) {
        Log.info("Parse cli");
        parseCli(args);
        PropertyConfigurator.configure(instance().getLogConfig());
        Log.info("Start application");
        createTestNG().run();
        Log.info("Finish app");
    }

    private static void parseCli(String[] args) {
        Log.info("Parsing clis using JCommander");
        JCommander jCommander = new JCommander(instance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error(e.getMessage());
            System.exit(1);
        }
        if (instance().isHelp()) {
            jCommander.usage();
        }
    }
}

package com.epam.gomel.tat.framework.test.yandex.product.disk;

import com.epam.gomel.tat.framework.bo.AccountFactory;
import com.epam.gomel.tat.framework.ui.browser.Browser;
import com.epam.gomel.tat.yandex.product.disk.screen.LoginPage;
import com.epam.gomel.tat.yandex.product.disk.services.AccountService;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTest {
    @BeforeClass
    public void openStartPage() {
        new LoginPage().open();
    }

    @Test(description = "Test that user cannot sign in with incorrect login and password")
    public void incorrectLogin() {
        new AccountService().signIn(AccountFactory.incorrectAccount());
        boolean isLogInSuccess = new LoginPage().isLogInSuccess();
        Assert.assertFalse(isLogInSuccess, "log in successfully with incorrect data");
    }

    @Test(dependsOnMethods = "incorrectLogin",
            description = "Test that user can sign in with correct login and password")
    public void correctLogin() {
        Browser.getInstance().refresh();
        new AccountService().signIn(AccountFactory.defaultAccount());
        boolean isLogInSuccess = new LoginPage().isLogInSuccess();
        Assert.assertTrue(isLogInSuccess, "Can't login with correct data");
    }
}

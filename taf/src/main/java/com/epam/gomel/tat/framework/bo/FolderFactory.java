package com.epam.gomel.tat.framework.bo;

import com.epam.gomel.tat.framework.config.Configuration;

public class FolderFactory {

    public static Folder defaultFolder() {
        Folder folder = new Folder();
        folder.setName(Configuration.NAME);
        return folder;
    }
}
